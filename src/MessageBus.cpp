/*

Copyright (c) 2020, CIMCON Software India Pvt. Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

// Declaration header
#include "MessageBus.hpp"
#include "StringEx.hpp"
#include "Environment.hpp"
#include "Directory.hpp"
#include "Base64.hpp"
#include "Buffer.hpp"

// C Headers
#include <mosquitto.h>
#include <limits.h>
#include <memory.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <malloc.h>
#include <memory.h>

#pragma pack(1)
typedef struct message_bus_t
{
    bool run;
    char process_name[64];
    struct mosquitto *mosq;
    int keep_alive;
    int payload_sequence;
    messabus_bus_callback callback;
    thread_t thread;
}message_bus_t;

static void message_bus_internal_connect_callback(struct mosquitto *mosq, void *obj, int result);
static void message_bus_internal_message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message);

static void* message_bus_internal_receiver_thread(void* ptr);

message_bus_t* message_bus_initialize(messabus_bus_callback cl)
{
    message_bus_t* message_bus_ptr = (message_bus_t*)calloc(1, sizeof(message_bus_t));

    if (message_bus_ptr == nullptr)
    {
        return nullptr;
    }

    int rc = 0;

    bool clean_session = true;

    message_bus_ptr->run = true;
    message_bus_ptr->keep_alive = 60;
    message_bus_ptr->callback = cl;
    message_bus_ptr->payload_sequence = 0;
    memset(message_bus_ptr->process_name, 0, 64);
    env_get_current_process_name(message_bus_ptr->process_name);
    rc = mosquitto_lib_init();

    if(rc != MOSQ_ERR_SUCCESS)
    {
        free(message_bus_ptr);
        return nullptr;
    }

    message_bus_ptr->mosq = mosquitto_new(message_bus_ptr->process_name, clean_session, message_bus_ptr);

    if(!message_bus_ptr->mosq)
    {
        free(message_bus_ptr);
        return nullptr;
    }

    return message_bus_ptr;
}

void message_bus_set_node_name(message_bus_t* ptr, const char* nodename)
{
    if(ptr == nullptr)
    {
        return;
    }

    memset(ptr->process_name, 0, 64);
    strncpy(ptr->process_name, nodename, 63);
}


bool message_bus_release(message_bus_t* ptr)
{
    if(ptr == nullptr)
    {
        return false;
    }

    if(ptr->mosq)
    {
        mosquitto_destroy(ptr->mosq);
    }

    mosquitto_lib_cleanup();

    free(ptr);

    return true;
}

bool message_bus_open(message_bus_t* ptr)
{
    if(ptr == nullptr)
    {
        return false;
    }

    mosquitto_message_callback_set(ptr->mosq, message_bus_internal_message_callback);
    mosquitto_connect_callback_set(ptr->mosq, message_bus_internal_connect_callback);

    int rc = 0;

    rc = mosquitto_connect(ptr->mosq, "127.0.0.1", 1883, ptr->keep_alive);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    pthread_attr_t pthread_attr;
    memset(&pthread_attr, 0, sizeof(pthread_attr_t));
    // default threading attributes
    pthread_attr_init(&pthread_attr);
    // allow a thread to exit cleanly without a join
    pthread_attr_setdetachstate (&pthread_attr, PTHREAD_CREATE_DETACHED);
    if (pthread_create(&ptr->thread, &pthread_attr, message_bus_internal_receiver_thread, (void*)ptr) != 0)
    {
        return false;
    }

    pthread_attr_destroy(&pthread_attr);

    char subscription_topic[33] = {0};

    memset(subscription_topic, 0, 33);
    strcat(subscription_topic, ptr->process_name);
    strcat(subscription_topic, "/+");
    rc = mosquitto_subscribe(ptr->mosq, nullptr, subscription_topic, 0);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    return true;
}


bool message_bus_close(message_bus_t* ptr)
{
    if(ptr == nullptr)
    {
        return false;
    }

    ptr->run = false;

    int rc = mosquitto_disconnect(ptr->mosq);

    if(rc != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    mosquitto_destroy(ptr->mosq);

    ptr->mosq = nullptr;

    return false;
}

// Messaging
bool message_bus_send(message_bus_t* ptr, const char* node_name, PayloadType ptype, DataType dtype, char* messagebuffer, long buffersize)
{
    if(ptr == nullptr)
    {
        return false;
    }

    ptr->payload_sequence++;
    if(ptr->payload_sequence > INT_MAX -1)
    {
        ptr->payload_sequence = 1;
    }

    // Take a big enough buffer
    //char publish_topic[64] = { 0 };
    //sprintf(publish_topic, "%s/%s:%c%c", node_name, ptr->process_name, ptype, dtype);

    char* topic_buffer = nullptr;
    int topic_buffer_len = strlen(node_name) + 1 + strlen(ptr->process_name) + 1 + +1 +1 +1;
    topic_buffer = (char*)calloc(1, topic_buffer_len);
    sprintf(topic_buffer, "%s/%s:%c%c", node_name, ptr->process_name, ptype, dtype);

    if(!topic_buffer)
    {
        return false;
    }

    int datalen = 0;
    char* dataptr = nullptr;

    if(dtype == Text || dtype ==SMSText)
    {
        dataptr = (char*)messagebuffer;
        datalen = (int)buffersize;
    }
    else
    {
        dataptr = base64_encode((const unsigned char*)messagebuffer, (unsigned long)buffersize, dataptr, (unsigned long*)&datalen);
        datalen = (int)strlen(dataptr);
    }

    if(mosquitto_publish(ptr->mosq, &ptr->payload_sequence, topic_buffer, datalen, dataptr,  2, 0) != MOSQ_ERR_SUCCESS)
    {
        free(topic_buffer);
        return false;
    }
    if(dtype == Text || dtype ==SMSText)
    {

    }
    else
    free(topic_buffer);

    if(dtype != Text)
    {
        free(dataptr);
    }

    return true;
}

void* message_bus_internal_receiver_thread(void* ptr)
{
    pthread_detach(pthread_self());

    message_bus_t* message_bus_ptr = (message_bus_t*)ptr;

    if(message_bus_ptr == nullptr)
    {
        return nullptr;
    }

    int rc = 0;

    while(message_bus_ptr->run)
    {
        rc = mosquitto_loop(message_bus_ptr->mosq, -1, 1);

        if(message_bus_ptr->run && rc)
        {
            sleep(5);

            rc = mosquitto_reconnect(message_bus_ptr->mosq);

            if(rc != MOSQ_ERR_SUCCESS)
            {
                message_bus_ptr->run = false;
                break;
            }
        }
    }

    pthread_exit(nullptr);

    return ptr;
}

/*------------------------------------------------------------*/
// Internal functions
// Node management

void message_bus_internal_connect_callback(struct mosquitto *mosq, void *obj, int result)
{
}

void message_bus_internal_message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
    message_bus_t* message_bus_ptr = (message_bus_t*)obj;

    if (message_bus_ptr == nullptr)
    {
        return;
    }

    size_t tlen = strlen(message->topic);

    DataType dtype = (DataType)message->topic[tlen-1];
    PayloadType ptype = (PayloadType)message->topic[tlen-2];
    message->topic[tlen-3] = 0;

    char topic[33] = {0};
    char sender[33] = {0};

    char** tokens = nullptr;

    tokens = strsplitchar(message->topic, '/');

    if(!tokens)
    {
        return;
    }

    if(!tokens[0])
    {
        return;
    }

    if(!tokens[1])
    {
        return;
    }

    strcpy(topic, tokens[0]);
    strcpy(sender, tokens[1]);

    strfreelist(tokens);

    if(message_bus_ptr->callback)
    {
        unsigned char* buffer = nullptr;
        unsigned long buffer_size = 0;

        if(dtype != Text &&  dtype !=SMSText)
        {
            buffer = base64_decode((const char*)message->payload, message->payloadlen, buffer, &buffer_size);
        }
        else
        {
            buffer = (unsigned char*)message->payload;
            buffer_size = (unsigned long)message->payloadlen;
        }

        message_bus_ptr->callback(sender, ptype, dtype, (char*)buffer, buffer_size, message->mid);

        if(dtype != Text && dtype != SMSText)
        {
            free(buffer);
        }
    }
}
