/*

Copyright (c) 2020, CIMCON Software India Pvt. Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef MESSAGE_BUS
#define MESSAGE_BUS

#include "Defines.hpp"

#ifdef __cplusplus
extern "C" {
#endif

// Enumerations
typedef enum PayloadType
{
    Data='D',
    Event='E',
    Request='Q',
    Response='R'
}PayloadType;

typedef enum DataType
{
    Text='T',
    Video='V',
    Image='I',
    Audio='A',
    Raw='R',
    SMSText='S'
}DataType;

typedef struct message_bus_t message_bus_t;

typedef void(*messabus_bus_callback)(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);

extern LIBRARY_EXPORT message_bus_t* message_bus_initialize(messabus_bus_callback callback);
extern LIBRARY_EXPORT void message_bus_set_node_name(message_bus_t* ptr, const char* nodename);
extern LIBRARY_EXPORT bool message_bus_release(message_bus_t* ptr);
extern LIBRARY_EXPORT bool message_bus_open(message_bus_t* ptr);
extern LIBRARY_EXPORT bool message_bus_close(message_bus_t* ptr);
extern LIBRARY_EXPORT bool message_bus_send(message_bus_t* ptr, const char* node_name, PayloadType ptype, DataType dtype, char* messagebuffer, long buffersize);

#ifdef __cplusplus
}
#endif

#endif

