/*

Copyright (c) 2020, CIMCON Software India Pvt. Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef DIRECTORY_C
#define DIRECTORY_C

#include "Defines.hpp"

#ifdef __cplusplus
extern "C" {
#endif

extern bool dir_is_exists(const char* dirname);
extern bool dir_create_directory(const char* dirname);
extern char* dir_get_parent_directory(const char* dirname);
extern char* dir_get_temp_directory(char* dirname);
extern char* dir_get_log_directory(char *dirname);
extern char* dir_get_config_directory(char *dirname);
extern char* dir_get_resource_directory(char *dirname);

#ifdef __cplusplus
}
#endif

#endif
