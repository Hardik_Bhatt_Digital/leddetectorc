/*

Copyright (c) 2020, CIMCON Software India Pvt. Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef LOGGER_C
#define LOGGER_C

#include "Defines.hpp"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum LogLevel
{
	LOG_INFO = 0,
	LOG_ERROR = 1,
	LOG_WARNING = 2,
	LOG_CRITICAL = 3,
	LOG_PANIC = 4
}LogLevel;

typedef struct logger_t logger_t;

extern logger_t*  logger_allocate_default();
extern logger_t*  logger_allocate(size_t flszmb, const char* dirpath);
extern logger_t*  logger_allocate_file(size_t flszmb, const char* filename);
extern const char* logger_filename(logger_t* loggerptr);
extern void    logger_release(logger_t* loggerptr);
extern bool    logger_write(logger_t* loggerptr, const char* logentry, LogLevel llevel, const char* func, const char* file, int line);
extern void logger_enable_console_out(logger_t* loggerptr, bool consoleout);
extern void logger_set_log_level(logger_t* loggerptr, LogLevel llevel);

#define WriteLog(lptr, str, level) \
    logger_write(lptr, str, level, __PRETTY_FUNCTION__, __FILE__, __LINE__)

#define WriteInformation(lptr, str) \
    logger_write(lptr, str, LOG_INFO, __PRETTY_FUNCTION__, __FILE__, __LINE__);

#ifdef __cplusplus
}
#endif

#endif

